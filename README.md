# nginx

## nginx as reverse proxy on docker

read more from [here](https://medium.com/rahasak/nginx-as-reverse-proxy-with-docker-c9ead938fffd)

## configure ssl with nginx

read more from [here](https://medium.com/@itseranga/set-up-ssl-certificates-on-nginx-c51f7dc00272)

## nginx reverse proxy with url rewrite

read more from [here](https://medium.com/rahasak/nginx-reverse-proxy-with-url-rewrite-a3361a35623c)

## setup let's encrypt certificate with nginx, certbot and docker

read more from [here](https://medium.com/@itseranga/setup-lets-encrypt-certificate-with-nginx-certbot-and-docker-b13010a12994)
