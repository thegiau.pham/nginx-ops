#!/bin/bash

# add crontab entry to renew the letsencrypt certificate
echo "adding crontab"
echo "0 23 * * * certbot renew --dry-run" | crontab -

# start nginx
echo "starting nginx"
nginx
