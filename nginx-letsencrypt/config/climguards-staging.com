server {
    listen 80;
    listen [::]:80;
    server_name     climguards.com www.climguards.com;

    location ~ /.well-known/acme-challenge {
        allow all;
        root /var/www/html;
    }
}
