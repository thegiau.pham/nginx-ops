server {
    listen          80;
    server_name     lekana.com www.lekana.com;
    location / {
        proxy_pass  http://lekana-api:7654;
    }
}
