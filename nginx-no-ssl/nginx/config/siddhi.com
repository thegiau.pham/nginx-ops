server {
    listen          80;
    server_name     siddhi.com www.siddhi.com;
    location / {
        proxy_pass  http://siddhi-api:7655;
    }
}
