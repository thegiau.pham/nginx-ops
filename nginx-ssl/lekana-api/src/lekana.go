package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
)

func main() {
	// router
	r := mux.NewRouter()
	r.HandleFunc("/api/lekana", apiLekana).Methods("GET")

	// start server
	err := http.ListenAndServe(":7654", r)
	if err != nil {
		log.Printf("ERROR: fail init http server, %s", err.Error)
		os.Exit(1)
	}
}

func apiLekana(w http.ResponseWriter, r *http.Request) {
	log.Printf("INFO: lekana api")

	fmt.Fprintln(w, "lekana")
}
