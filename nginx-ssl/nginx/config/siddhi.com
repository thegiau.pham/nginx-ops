server {
    listen          443 ssl;
    server_name     siddhi.com www.siddhi.com;

    ssl_certificate     /etc/nginx/certs/server.crt;
    ssl_certificate_key /etc/nginx/certs/server.key;

    location / {
        proxy_pass  http://siddhi-api:7655;
    }
}
